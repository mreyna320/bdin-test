import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
	selector: 'app-formulario',
	templateUrl: './formulario.component.html',
	styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
	formularioIncompleto = true;
	datosPaciente = new DatosPaciente();
	listaEstadoCivil = [
		{ value: "S", viewValue: "Soltero/a" },
		{ value: "V", viewValue: "Viudo/a" },
		{ value: "C", viewValue: "Casado/a" },
		{ value: "D", viewValue: "Divorciado/a" }
	]
	timeoutValidacion;

	constructor() { }

	ngOnInit() {
	}

	validarFormulario(): void {
		clearTimeout(this.timeoutValidacion);
		this.timeoutValidacion = setTimeout(() => {
			let dc = this.datosPaciente.datosContacto;
			let dp = this.datosPaciente.datosPersonales;
			if (
				!dc.direccion || (!dc.email && !dc.telefono) ||
				!dp.nombre || !dp.apellido || !dp.documento
			) {
				this.formularioIncompleto = true
			} else {
				this.formularioIncompleto = false;
			}
		}, 300);
	}

	guardar() {
		//Lugar para insertar validaciones
		alert("Datos guardados correctamente");
		this.datosPaciente = new DatosPaciente();
	}


}

class DatosPaciente {
	constructor() {
		this.datosPersonales = new DatosPersonales();
		this.datosContacto = new DatosContacto();
	}

	datosPersonales: DatosPersonales;
	datosContacto: DatosContacto;
}

class DatosPersonales {
	nombre: string;
	apellido: string;
	fechaNacimiento: string;
	estadoCivil: string;
	documento: number;
}
class DatosContacto {
	direccion: string;
	telefono: string;
	email: string;
}