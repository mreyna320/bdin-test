import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioComponent } from './formulario.component';
import { FormsModule } from '@angular/forms';
import { element } from 'protractor';
import { By } from '@angular/platform-browser';

describe('FormularioComponent', () => {
  let component: FormularioComponent;
  let fixture: ComponentFixture<FormularioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormularioComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe deshabilitar el boton guardar cuando estan incompletos campos obligatorios', (done) => {
    component.validarFormulario();
    fixture.detectChanges();

    setTimeout(() => {
      const el = fixture.debugElement.nativeElement.querySelector("button");
      expect(el.disabled).toBe(true);
      done();
    }, 500);
  });

  it('Debe habilitar el boton guardar cuando estan completos campos obligatorios', (done) => {
    component.datosPaciente.datosContacto = { telefono: "22333", email: "ss@ss", direccion: "Cordoba 2233" };
    component.datosPaciente.datosPersonales = { nombre: "test", apellido: "bdin", documento: 55666, estadoCivil: null, fechaNacimiento: "30/05/1990" }
    component.validarFormulario();
    fixture.detectChanges();

    setTimeout(() => {
      expect(component.formularioIncompleto).toBe(false);
      done();
    }, 1000);
  });

});
