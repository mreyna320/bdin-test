import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-listado',
	templateUrl: './listado.component.html',
	styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
	private timeoutBusqueda;

	public listadoFiltrado: Array<Sucursales> = [];

	public listadoCompleto: Array<Sucursales> = [
		{
			titulo: 'La glorieta',
			direccion: 'Gurruchaga 1212, Palermo',
			texto: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a rutrum ligula. Fusce eros mauris, porttitor ut dui id, condimentum rutrum justo. In sem mauris, bibendum at rutrum lobortis, hendrerit vel sapien. Nullam urna augue, gravida eu viverra sed, faucibus eu augue. Aliquam a augue molestie, aliquam ex nec, bibendum dolor. Sed quis nisi at tortor sodales lobortis. Sed maximus felis ut diam commodo, at condimentum erat dapibus. Ut semper, lectus in sodales sagittis, leo leo pulvinar ex, in facilisis tortor erat congue massa. Maecenas malesuada ornare facilisis. Pellentesque luctus pharetra nulla, eget gravida velit mollis non.",
			imagen: 'assets/osde.png'
		},
		{
			titulo: 'Asistencia medica',
			direccion: 'Thames 655, Villa Crespo',
			texto: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a rutrum ligula. Fusce eros mauris, porttitor ut dui id, condimentum rutrum justo. In sem mauris, bibendum at rutrum lobortis, hendrerit vel sapien. Nullam urna augue, gravida eu viverra sed, faucibus eu augue. Aliquam a augue molestie, aliquam ex nec, bibendum dolor. Sed quis nisi at tortor sodales lobortis. Sed maximus felis ut diam commodo, at condimentum erat dapibus. Ut semper, lectus in sodales sagittis, leo leo pulvinar ex, in facilisis tortor erat congue massa. Maecenas malesuada ornare facilisis. Pellentesque luctus pharetra nulla, eget gravida velit mollis non.",
			imagen: 'assets/osde.png'
		},
		{
			titulo: 'Dr. Sanchez',
			direccion: 'Avellaneda 908, Caballito',
			texto: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a rutrum ligula. Fusce eros mauris, porttitor ut dui id, condimentum rutrum justo. In sem mauris, bibendum at rutrum lobortis, hendrerit vel sapien. Nullam urna augue, gravida eu viverra sed, faucibus eu augue. Aliquam a augue molestie, aliquam ex nec, bibendum dolor. Sed quis nisi at tortor sodales lobortis. Sed maximus felis ut diam commodo, at condimentum erat dapibus. Ut semper, lectus in sodales sagittis, leo leo pulvinar ex, in facilisis tortor erat congue massa. Maecenas malesuada ornare facilisis. Pellentesque luctus pharetra nulla, eget gravida velit mollis non.",
			imagen: 'assets/osde.png'
		},
		{
			titulo: 'Sanatorio Morales',
			direccion: 'Thames 415, Moron',
			texto: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a rutrum ligula. Fusce eros mauris, porttitor ut dui id, condimentum rutrum justo. In sem mauris, bibendum at rutrum lobortis, hendrerit vel sapien. Nullam urna augue, gravida eu viverra sed, faucibus eu augue. Aliquam a augue molestie, aliquam ex nec, bibendum dolor. Sed quis nisi at tortor sodales lobortis. Sed maximus felis ut diam commodo, at condimentum erat dapibus. Ut semper, lectus in sodales sagittis, leo leo pulvinar ex, in facilisis tortor erat congue massa. Maecenas malesuada ornare facilisis. Pellentesque luctus pharetra nulla, eget gravida velit mollis non.",
			imagen: 'assets/osde.png'
		}
	];
	
	constructor() { }

	ngOnInit() {
		this.listadoFiltrado.push(...this.listadoCompleto);
	}

	buscar(val: string) {
		clearTimeout(this.timeoutBusqueda);
		this.timeoutBusqueda = setTimeout((val) => {
			this.listadoFiltrado = [];
			if (!val) {
				this.listadoFiltrado.push(...this.listadoCompleto);
				return;
			}
			let auxArr = new Array<Sucursales>();
			for (const item of this.listadoCompleto) {
				if (item.direccion.match(val)) {
					auxArr.push(item);
				}
			}
			this.listadoFiltrado.push(...auxArr);
		}, 300, val);
	}
}

class Sucursales {
	texto: string;
	imagen: string;
	titulo: string;
	direccion: string;
}