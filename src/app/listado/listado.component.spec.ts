import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { ListadoComponent } from './listado.component';

describe('ListadoComponent', () => {
	let component: ListadoComponent;
	let fixture: ComponentFixture<ListadoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ListadoComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ListadoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('El listadoFiltrado INICIAL debe ser igual al listadoCompleto', () => {
		let sonIguales = true;
		sonIguales = (component.listadoCompleto.length == component.listadoFiltrado.length);
		if (sonIguales) {
			for (let i = 0; i < component.listadoCompleto.length; i++) {
				let itComp = component.listadoCompleto;
				let itFiltr = component.listadoFiltrado;
				if (
					itComp[i].direccion != itFiltr[i].direccion ||
					itComp[i].imagen != itFiltr[i].imagen ||
					itComp[i].texto != itFiltr[i].texto ||
					itComp[i].titulo != itFiltr[i].titulo
				) {
					sonIguales = false;
					break;
				}
			}

		}

		expect(sonIguales).toBe(true);
	});

	it('No debe tener valores el listadoFiltrado al buscar un valor inexistente', (done) => {
		component.buscar("VALOR DUMMY INEXISTENTE EN LOS DATOS");
		fixture.detectChanges();

		setTimeout(() => {
			expect(component.listadoFiltrado.length).toBe(0);
			done();
		}, 500);
	});
});
