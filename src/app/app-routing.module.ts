import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoComponent } from './listado/listado.component';
import { FormularioComponent } from './formulario/formulario.component';


const routes: Routes = [
	{ path: 'listado', component: ListadoComponent },
	{ path: 'formulario', component: FormularioComponent },
	{ path: '', component: FormularioComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
